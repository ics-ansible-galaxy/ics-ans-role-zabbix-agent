import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_config_file_exists(host):
    file = host.file('/etc/zabbix/zabbix_agentd.conf')
    assert file.exists


def test_snmpd_enabled_and_running(host):
    service = host.service('zabbix-agent')
    assert service.is_running
    assert service.is_enabled
