ics-ans-role-zabbix-agent
===================

Ansible role to install zabbix-agent on Linux servers.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
# Set Zabbix server or proxy hostname variable 
zabbix_servername: hostname.domain.com

# Set and enable custom scripts variables
zabbix_custom_scripts: []

# Example list:
zabbix_custom_scripts:
  - zabbix_script: 'service_discovery'
    zabbix_srv_conf: "service_discovery.conf"
    enabled: true
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zabbix-agent
```

License
-------

BSD 2-clause
---
